<?php

namespace Drupal\cer;

final class CorrespondingReferenceOperations {
  const ADD = 'add';

  const REMOVE = 'remove';
}
